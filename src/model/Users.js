const Users = [
    {
        id: 1,
        name: 'Mario',
        email: 'user1@email.com',
        password: 'password',
        userToken: 'token123',
    },
    {
        id: 2,
        name: 'Bruce',
        email: 'user2@email.com',
        password: 'pass123',
        userToken: 'token12345',
    },
    {
        id: 3,
        name: 'Tony',
        email: 'testuser@email.com',
        password: 'testpass',
        userToken: 'testtoken',
    },
];

export default Users;