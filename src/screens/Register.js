import React, { useState } from 'react';
import { 
  Alert, 
  Image, 
  Text, 
  ScrollView, 
  View, 
  StyleSheet, 
  TextInput, 
  TouchableOpacity 
} from 'react-native';
import ImagePicker from 'react-native-image-crop-picker';
import Feather from 'react-native-vector-icons/Feather';
import * as Animatable from 'react-native-animatable';

import {AuthContext} from '../components/Context';

export function Register ({ navigation }) {

  const { signUp } = React.useContext(AuthContext);

  const [regData, setRegData] = React.useState({
    name: '',
    email: '',
    check_nameInputChange: false,
    check_emailInputChange: false,
    isValidName: true,
    isValidImage: true,
    isValidEmail: true,
  });

  const [image, setImage] = useState(null)
  const [images, setImages] = useState(null)

  const pickSingle = (cropit, circular = true, mediaType) => {
    ImagePicker.openPicker({
      width: 500,
      height: 500,
      cropping: cropit,
      cropperCircleOverlay: circular,
      sortOrder: 'none',
      compressImageMaxWidth: 1000,
      compressImageMaxHeight: 1000,
      compressImageQuality: 1,
      compressVideoPreset: 'MediumQuality',
      includeExif: true,
      cropperStatusBarColor: 'white',
      cropperToolbarColor: 'white',
      cropperActiveWidgetColor: 'white',
      cropperToolbarWidgetColor: '#3498DB',
    })
      .then((image) => {
        console.log('received image', image);
        setImage({
            uri: image.path,
            width: image.width,
            height: image.height,
            mime: image.mime,
        })
        setImages(null);
      })
      .catch((e) => {
        console.log(e);
        Alert.alert(e.message ? e.message : e);
      });
  }

  const renderAsset = (image) => {
    return renderImage(image);
  }

  const renderImage = (image) => {
    return (
      <Image 
        style={{
          width: 120, 
          height: 120, 
          resizeMode: 'contain', 
          borderRadius: 100
        }} 
        source={image}
      />
    );
  }

  const nameInputChange = (val) => {
    if (val.trim().length >= 3) {
      setRegData({
        ...regData,
        name: val,
        check_nameInputChange: true
      });
    } else {
      setRegData({
        ...regData,
        name: val,
        check_nameInputChange: false
      });
    }
  }

  const emailInputChange = (val) => {
    var pattern = /^[a-zA-Z0-9\-_]+(\.[a-zA-Z0-9\-_]+)*@[a-z0-9]+(\-[a-z0-9]+)*(\.[a-z0-9]+(\-[a-z0-9]+)*)*\.[a-z]{2,4}$/;
    if (val.trim().length >= 3 && pattern.test(val)) {
      setRegData({
        ...regData,
        email: val,
        check_emailInputChange: true,
        isValidUser: true,
      });
    } else {
      setRegData({
        ...regData,
        email: val,
        check_emailInputChange: false,
        isValidEmail: false,
      });
    }
  }

  const registerHandle = () => {
    if(regData.email.trim().length == 0 || regData.name.trim().length == 0) {
      Alert.alert('Invalid Input!', 'Employee Name or Email cannot be empty', [
        {text: 'Okay'}
      ]);
      return;
    }
    signUp();
  }

  return (
    <View style={styles.container}>

      {/* Page Header */}
      <View style={styles.header}>
        <Image 
            source={require('../images/neoscrumLogo.png')}
            style={styles.logo}
        />
        <Text
          style={styles.title}
        >
          ENTER A NEW DEVELOPER
        </Text>
      </View>
      {/* Page Header End */}

      {/* User Image */}
      <ScrollView>
        {image? renderAsset(image): null}
        {images ? images.map((i) => (
          <View key={i.uri}>{this.renderAsset(i)}</View>
        ))
        : null}
      </ScrollView>
      {/* User Image End */}

      <View style={styles.body}>
        <Text style={styles.fieldTitle}>Employee Name</Text>
        <View style={styles.action}>
          <Feather
            name='user'
            color='#05375a'
            size={20}
          />
          <TextInput 
            style={styles.input} 
            placeholder='Enter Name'
            onChangeText={(val) => nameInputChange(val)}
          />
          { regData.check_nameInputChange ?
          <Animatable.View
            animation='bounceIn'
          >
            <Feather 
              name='check-circle'
              color='green'
              size={20}
            />
          </Animatable.View>
          : null }
        </View>

        <Text style={styles.fieldTitle}>Email</Text>
        <View style={styles.action}>
          <Feather
            name='mail'
            color='#05375a'
            size={20}
          />
          <TextInput 
            style={styles.input} 
            placeholder='Enter Email'
            keyboardType='email-address'
            autoCapitalize='none'
            onChangeText={(val) => emailInputChange(val)}
          />
          { regData.check_emailInputChange ?
          <Animatable.View
            animation='bounceIn'
          >
            <Feather 
              name='check-circle'
              color='green'
              size={20}
            />
          </Animatable.View>
          : null }
        </View>

        <TouchableOpacity
          style={styles.imageButton}
          onPress={() => pickSingle(true, true)}
        >
          <Text style={styles.buttonText}>Select Image</Text>
        </TouchableOpacity>
      </View>

      <View style={styles.buttonGroup}>
        <TouchableOpacity 
          style={styles.loginButton}
          onPress={() => {registerHandle(regData.name, regData.email)}}
        >
          <Text style={styles.buttonText}>REGISTER</Text>
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => navigation.pop()}
        >
          <Text style={styles.textButtonText}>Already a User? Login Here!</Text>
        </TouchableOpacity>
      </View>
    </View>
  )
}

const styles = StyleSheet.create ({
  container: {
    flex: 1,
    padding: 20,
    paddingTop: 75,
    alignItems: 'center',
  },
  header: {
    alignItems: 'center'
  },
  logo: {
    width: 200,
    height: 32,
    marginBottom: 28,
  },
  title: {
    fontSize: 28,
    marginBottom: 36,
  },
  body: {
    width: '100%',
  },
  fieldTitle: {
    fontSize: 18,
    color: '#bc0000'
  },
  action: {
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomColor: '#f2f2f2',
    borderBottomWidth: 1,
    marginTop: -10,
    marginBottom: 35,
  },
  input: {
    flex: 1,
    paddingLeft: 10,
    color: '#707070',
  },
  loginButton: {
    marginTop: 32,
    marginBottom: 24,
    backgroundColor: '#8e0101',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
    borderRadius: 10,
  },
  buttonText: {
    color: 'white',
    fontWeight: '500',
    fontSize: 16,
  },
  imageButton: {
    padding: 15,
    borderRadius: 10,
    marginTop: 10,
    marginBottom: 16,
    width: '50%',
    backgroundColor: '#8e0101',
    alignItems: 'center'
  },
  textButtonText: {
    color: '#BC0000',
    fontWeight: '500',
    fontSize: 14,
  },
  buttonGroup: {
    width: '100%',
    alignItems: 'center',
  },
})