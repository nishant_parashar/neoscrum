import React from 'react';
import { View, StyleSheet, Text, ScrollView } from 'react-native';
import { Header } from '../components/Header';
import FeedbackPost from '../components/FeedbackPost';

export function Dashboard () {

    const feedback = [
        {
            id: 1,
            comment: 'Your code is understandable'
        },
        {
            id:2,
            comment: 'You grasp concepts quickly'
        },
        {
            id: 3,
            comment: 'Your designs are nice'
        }
    ]

    const posts = feedback.map(item => (
        <View key={item.id}>
            <FeedbackPost value = {item.comment} />
        </View>
    ))

    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <Header />
            </View>
            <View style={styles.body}>
                <View>
                    <Text style={styles.title}>Feedbacks</Text>
                </View>
                <ScrollView>
                    <FeedbackPost feedback={feedback} />
                </ScrollView>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    header: {
        flex: 1,
    },
    body: {
        flex: 9,
        paddingTop: 15,
        alignItems: 'center'
    },
    title: {
        fontSize: 24,
    },
})