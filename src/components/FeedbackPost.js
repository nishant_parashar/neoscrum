import React, { useEffect, useState } from 'react';
import { View, Text, StyleSheet, ActivityIndicator } from 'react-native';

const FeedbackPost = (props) => {

    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        setTimeout(() => {
            setIsLoading(false);
        }, 1000)
    }, [])
    return (
        isLoading ? (
            <ActivityIndicator color='#bc0000' />
        )
        :
        (
            <View style={styles.container}>
                {
                    props.feedback.map(item => {
                        return (
                            <View style={styles.post} key={item.id}>
                                <View style={styles.postHeader}>
                                    <Text style={styles.feedbackTitle}>Feedback</Text>
                                </View>
                                <View style={styles.feedbackBody}>
                                    <Text style={{fontSize:16,}}>{item.comment}</Text>
                                </View>
                            </View>
                        )
                    })
                }
            </View>
        )
    );
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        justifyContent:'center',
        alignItems:'center',
        padding:10
    },
    post: {
        height: 200,
        width:350,
        marginBottom:20,
        borderWidth:1,
        borderColor: '#e4e4e4',
        borderRadius: 10,
    },
    postHeader : {
        padding:10,
        backgroundColor: '#A30505',
        margin: 0,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
    },
    feedbackTitle: {
        color: '#fff',
        fontSize: 20,
    },
    feedbackBody : {
        padding:10,
        margin: 10
    }
})

export default FeedbackPost;