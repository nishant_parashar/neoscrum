import React, { useState } from 'react';
import { View, Text, StyleSheet, TextInput, TouchableOpacity, Image, Alert } from 'react-native';
import { Formik } from 'formik';

const AddfeedbackPost =  ({feedbackPost}) => {
    const [message, setMessage] = useState('');

    const handleFeedbackSubmit = () => {
        Alert.alert('Feedback', message, [
            {text: 'Okay'}
        ]);
        setMessage('')
    }

    const handleMessage = (value) => {
        setMessage(value)
    }

    return (
        <View style={styles.container}>
            {
                feedbackPost.map(item => {
                    return (
                        <View key={item.id} style={styles.addFeedbackContainer}>
                            <Image
                                source={require('../images/profilePic.jpeg')}
                                style={styles.profileStyles}
                            />
                            <Text style={styles.userName}>{item.name}</Text>

                            <Formik
                                initialValues={{message: ''}}
                                onSubmit = {(values,{resetForm}) => {
                                    handleMessage(values);
                                    resetForm();
                                }}
                            >
                                {({handleChange, handleBlur, handleSubmit, values}) => (
                                    <View style={{width: '90%'}}>
                                        <TextInput
                                            // elevation={10}
                                            placeholder='Write your feedback here...'
                                            style={styles.inputStyles}
                                            onChangeText={handleChange('message')}
                                            onBlur={handleBlur('message')}
                                            value={values.message}
                                            maxLength={100}
                                        />
                                        <View style={styles.charcount}>
                                            <Text style={styles.counttext}>Max 100 characters</Text>
                                            <Text style={styles.counttext}>{message.length}/100</Text>
                                        </View>
                                        <TouchableOpacity style={styles.submitButton} onPress={handleSubmit}>
                                            <Text style={styles.submitText}>Give Feedback</Text>
                                        </TouchableOpacity>
                                    </View>
                                )}

                            </Formik>
                        </View>
                    )
                })
            }
        </View>
    )
}

const styles = StyleSheet.create({
    container : {
        flex:1,
    },
    profileStyles: {
        height: 100,
        width:100,
        borderRadius:50,
        marginBottom: 5,
    },
    userName: {
        color: '#eee',
        fontSize: 18,
    },
    addFeedbackContainer: {
        width: 350,
        height: 320,
        alignItems:'center',
        backgroundColor: '#A30505',
        marginTop:10,
        marginBottom:20,
        padding:10,
        borderRadius: 20,
    },
    inputStyles : {
        padding: 10,
        width: '100%',
        borderColor: '#e4e4e4',
        borderWidth:1,
        borderRadius: 10,
        marginTop: 25,
        backgroundColor: 'white',
    },
    charcount: {
        paddingVertical: 5,
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'space-between',
        marginBottom: 10,
    },
    counttext: {
        color: '#e4e4e4',
        fontSize: 12,
    },
    submitButton: {
        width: '100%',
        height: '25%',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
        borderRadius: 15,
        marginBottom: 0,
    },
    submitText: {
        fontSize: 17,
        color: '#a30505',
        fontWeight:'500',
    },
})

export default AddfeedbackPost;